<!-- Start Features69 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A variety of content based on alignment. -->
@endif
<section class="{{ $block->classes }}" is="fir-features-69" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="features-69 {{ $theme }} {{ $text_align }} {{ $flip }}">
    @if($title)
    <h1>{{ $title }}</h1>
    @endif
    @if($text)
    <p>{{ $text }}</p>
    @endif
    @if($link_primary)
    <a class="btn btn--full" href="{{ $link_primary['url'] }}">{{ $link_primary['title'] }}</a>
    @endif
    @if($link_secondary)
    <a class="btn btn--secondary" href="{{ $link_secondary['url'] }}">{{ $link_secondary['title'] }}</a>
    @endif
    @if($image)
    <img src="{{ $image['url'] }}" alt="{{ $title }}">
    @endif
  </div>
</section>
<!-- End Features69 -->
