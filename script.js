class Features69 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures69()
    }

    initFeatures69 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features69")
    }

}

window.customElements.define('fir-features-69', Features69, { extends: 'section' })
