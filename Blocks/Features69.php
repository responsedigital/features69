<?php

namespace Fir\Pinecones\Features69\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Features69 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Content Centered';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Features69.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['Features69'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
          'title' => null,
          'text' => null,
          'image' => null,
          'link_primary' => null,
          'link_secondary' => null
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['title'] = ($data['content']['title']) ?: $this->defaults['content']['title'];
        $data['text'] = ($data['content']['text']) ?: $this->defaults['content']['text'];
        $data['image'] = ($data['content']['image']) ?: $this->defaults['content']['image'];
        $data['link_primary'] = ($data['content']['link_primary']) ?: $this->defaults['content']['link_primary'];
        $data['link_secondary'] = ($data['content']['link_secondary']) ?: $this->defaults['content']['link_secondary'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'features-69--flip' : '';
        $data['text_align'] = 'features-69--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Features69 = new FieldsBuilder('Basic Content');

        $Features69
            ->addGroup('content', [
                'label' => 'Features69',
                'layout' => 'block'
            ])
                ->addText('title')
                ->addTextArea('text')
                ->addImage('image')
                ->addLink('link_primary')
                ->addLink('link_secondary')
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Features69->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Features69/style.css')->uri(), false, null);
    }
}
